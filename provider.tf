provider "azurerm" {
  features {}
}

terraform {

 backend "azurerm" {
   resource_group_name = "tfstate"
   storage_account_name = "tfstatedussaussois"
   container_name = "tfstate"
   key = "TomDuss.terraform.tfstate"
 }
}